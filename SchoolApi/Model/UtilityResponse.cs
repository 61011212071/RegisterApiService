﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolApi.Model
{
    public class UtilityResponse<T>
    {
        public int status { get; set; }
        public string messageCode { get; set; }
        public string message { get; set; }
        public T data { get; set; }
    }
    public class UserMessage
    {
        public string messageTH { get; set; }
        public string messageEN { get; set; }
    }
}
