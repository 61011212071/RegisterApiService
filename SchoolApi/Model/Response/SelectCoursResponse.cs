﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolApi.Model.Response
{
    public class SelectCoursResponse
    {
        public List<Courses> courses { get; set; }

        public class Courses
        {
            public string Course_Code { get; set; }
            public string Course_name { get; set; }
            public int Student_Count { get; set; }
        }
    }
}
