﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolApi.Model.Response
{
    public class SelectAllStudentResponse
    {
        public List<Not_Register> not_Registers { get; set; }
        public List<Is_Register> is_Registers { get; set; }
        
        public class Not_Register {
            public string students_id { get; set; }
            public string students_name { get; set; }
            public string students_phone { get; set; }
            public string students_email { get; set; }
        }
        public class Is_Register {
            public string students_ID { get; set; }
            public string students_Name { get; set; }
            public string students_Phone { get; set; }
            public string students_Email { get; set; }
            public string course_Code { get; set; }
            public string course_Name { get; set; }
        }
       
    }
}
