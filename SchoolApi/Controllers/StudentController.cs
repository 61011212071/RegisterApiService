﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using SchoolApi.Model;
using SchoolApi.Model.Response;
using SchoolApi.Service;
using System.Data.SqlClient;
using SchoolApi.Model.ReQuses;

namespace SchoolApi.Controllers
{

    [Route("api/student")]
    [ApiController]
    [Consumes("application/json")]
    public class StudentController : Controller
    {
        StudentsService _service = new StudentsService();

        [HttpGet("selectallstudent")]
        public async Task<ActionResult> selectallstudent()
        {

            UtilityResponse<SelectAllStudentResponse> response = new UtilityResponse<SelectAllStudentResponse>();

            try
            {
                SelectAllStudentResponse selectAll = new SelectAllStudentResponse();

                selectAll = await _service.SelectAllStudent();

                response.status = 200;
                response.messageCode = "SUS200";
                response.message = "SUSCCESS";
                response.data = selectAll;
            }
            catch (Exception ex)
            {
                response.status = 404;
                response.messageCode = "ERROR404";
                response.message = ex.ToString();

            }

            return Ok(response);
        }

        [HttpPost("insertstudent")]
        public async Task<ActionResult> insertstudent(InsertStudentRequse requse)
        {
            UtilityResponse<int> response = new UtilityResponse<int>();
            UtilityResponse<int> responses = new UtilityResponse<int>();
            try
            {

                response = await _service.InsertStudent(requse);

                if (response.message.ToString().Equals("SUCCESS"))
                {
                    responses.status = 200;
                    responses.messageCode = "INSERTSUS";
                    responses.message = response.message.ToString();
                    responses.data = response.data;
                }
                else
                {
                    responses.status = 200;
                    responses.messageCode = "INSERTERROR";
                    responses.message = response.message.ToString();
                }
            }
            catch (Exception ex)
            {
                response.status = 404;
                response.messageCode = "ERROR404";
                response.message = ex.ToString();
            }

            return Ok(responses);
        }

        [HttpPost("editstudent")]
        public async Task<ActionResult> editstudent(EditStudent request)
        {
            UtilityResponse<int> response = new UtilityResponse<int>();
            UtilityResponse<int> responses = new UtilityResponse<int>();
            try
            {

                response = await _service.EditStudent(request);

                if (response.message.ToString().Equals("SUCCESS"))
                {
                    responses.status = 200;
                    responses.messageCode = "UTDATESUS";
                    responses.message = response.message.ToString();
                    responses.data = response.data;
                }
                else
                {
                    responses.status = 200;
                    responses.messageCode = "UTDATEERROR";
                    responses.message = response.message.ToString();
                }
            }
            catch (Exception ex)
            {
                response.status = 404;
                response.messageCode = "ERROR404";
                response.message = ex.ToString();
            }

            return Ok(responses);
        }

        [HttpPost("register")]
        public async Task<ActionResult> register(RegisterRequste request)
        {
            UtilityResponse<int> response = new UtilityResponse<int>();
            UtilityResponse<int> responses = new UtilityResponse<int>();
            try
            {

                response = await _service.Register(request);

                if (response.message.ToString().Equals("SUCCESS"))
                {
                    responses.status = 200;
                    responses.messageCode = "REGISTERSUS";
                    responses.message = response.message.ToString();
                    responses.data = response.data;
                }
                else
                {
                    responses.status = 200;
                    responses.messageCode = "REGISTERERROR";
                    responses.message = response.message.ToString();
                }
            }
            catch (Exception ex)
            {
                response.status = 404;
                response.messageCode = "ERROR404";
                response.message = ex.ToString();
            }

            return Ok(responses);
        }

        [HttpPost("insertcourse")]
        public async Task<ActionResult> insertcourse(InsertCourse request)
        {
            UtilityResponse<int> response = new UtilityResponse<int>();
            UtilityResponse<int> responses = new UtilityResponse<int>();
            try
            {

                response = await _service.InsertCourse(request);

                if (response.message.ToString().Equals("SUCCESS"))
                {
                    responses.status = 200;
                    responses.messageCode = "insertcourseSUS";
                    responses.message = response.message.ToString();
                    responses.data = response.data;
                }
                else
                {
                    responses.status = 200;
                    responses.messageCode = "insertcourseERROR";
                    responses.message = response.message.ToString();
                }
            }
            catch (Exception ex)
            {
                response.status = 404;
                response.messageCode = "ERROR404";
                response.message = ex.ToString();
            }

            return Ok(responses);
        }
        [HttpPost("editcourses")]
        public async Task<ActionResult> editcourses(InsertCourse request)
        {
            UtilityResponse<int> response = new UtilityResponse<int>();
            UtilityResponse<int> responses = new UtilityResponse<int>();
            try
            {

                response = await _service.EditCourses(request);

                if (response.message.ToString().Equals("SUCCESS"))
                {
                    responses.status = 200;
                    responses.messageCode = "editcourseSUS";
                    responses.message = response.message.ToString();
                    responses.data = response.data;
                }
                else
                {
                    responses.status = 200;
                    responses.messageCode = "editcourseERROR";
                    responses.message = response.message.ToString();
                }
            }
            catch (Exception ex)
            {
                response.status = 404;
                response.messageCode = "ERROR404";
                response.message = ex.ToString();
            }

            return Ok(responses);
        }
        [HttpGet("selectcourses")]
        public async Task<ActionResult> selectcourses()
        {

            UtilityResponse<SelectCoursResponse> response = new UtilityResponse<SelectCoursResponse>();

            try
            {
                SelectCoursResponse selectCours = new SelectCoursResponse();

                selectCours = await _service.SelectCourses();

                response.status = 200;
                response.messageCode = "SUS200";
                response.message = "SUSCCESS";
                response.data = selectCours;
            }
            catch (Exception ex)
            {
                response.status = 404;
                response.messageCode = "ERROR404";
                response.message = ex.ToString();

            }

            return Ok(response);
        }
    }
}
