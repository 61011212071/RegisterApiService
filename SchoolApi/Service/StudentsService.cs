﻿using Microsoft.Extensions.Configuration;
using SchoolApi.Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using SchoolApi.Model;
using SchoolApi.DB;
using SchoolApi.Model.ReQuses;
using System.Text.RegularExpressions;

namespace SchoolApi.Service
{
    public class StudentsService
    {

        public List<SqlParameter> Parameters;


        public string _connectionString = "Data Source=DESKTOP-AOFHV3I;Initial Catalog=school;Integrated Security=True";

        //SqlConnection _connectionString = new SqlConnection("Data Source=DESKTOP-AOFHV3I;Initial Catalog=school;Integrated Security=True");

        



        public async Task<SelectAllStudentResponse> SelectAllStudent() {

            SelectAllStudentResponse serviceRespone = new SelectAllStudentResponse();

            try {

                
                DataSet table = new DataSet();

                SqlExecute sql = new SqlExecute("sp_Student", _connectionString);

                SqlParameter key = new SqlParameter(@"Key",SqlDbType.VarChar, 50);
                key.Direction = ParameterDirection.Input;
                key.Value = "SelectAllStudent";
                sql.Parameters.Add(key);

                table = sql.executeReturnManyTable();

                DataRow checkStatus = table.Tables[0].Rows[0];
                if (CheckNull(checkStatus["STATUS"].ToString()).Equals("SUCCESS")) {

                    if (table != null && table.Tables[1].Rows.Count > 0)
                    {

                        SelectAllStudentResponse.Not_Register not_Register = new SelectAllStudentResponse.Not_Register();
                        serviceRespone.not_Registers = new List<SelectAllStudentResponse.Not_Register>();

                        foreach (DataRow row in table.Tables[1].Rows)
                        {

                            not_Register = new SelectAllStudentResponse.Not_Register();

                            not_Register.students_id = CheckNull(row["student_ID"].ToString());
                            not_Register.students_name = CheckNull(row["student_Name"].ToString());
                            not_Register.students_phone = CheckNull(row["student_phone"].ToString());
                            not_Register.students_email = CheckNull(row["student_email"].ToString());

                            serviceRespone.not_Registers.Add(not_Register);

                        }
                    }
                    if (table != null && table.Tables[2].Rows.Count > 0)
                    {

                        SelectAllStudentResponse.Is_Register is_Register = new SelectAllStudentResponse.Is_Register();
                        serviceRespone.is_Registers = new List<SelectAllStudentResponse.Is_Register>();

                        foreach (DataRow row in table.Tables[2].Rows)
                        {

                            is_Register = new SelectAllStudentResponse.Is_Register();

                            is_Register.students_ID = CheckNull(row["student_ID"].ToString());
                            is_Register.students_Name = CheckNull(row["student_Name"].ToString());
                            is_Register.students_Phone = CheckNull(row["student_phone"].ToString());
                            is_Register.students_Email = CheckNull(row["student_email"].ToString());
                            is_Register.course_Code = CheckNull(row["Course_code"].ToString());
                            is_Register.course_Name = CheckNull(row["Course_name"].ToString());

                            serviceRespone.is_Registers.Add(is_Register);

                        }
                    }


                }




            }
            catch (Exception ex) {

                throw ex;
            }

            return serviceRespone;
        }

        public async Task<UtilityResponse<int>> InsertStudent(InsertStudentRequse requse) {

            UtilityResponse<int> response = new UtilityResponse<int>();

            try {

                DataSet table = new DataSet();
                SqlExecute sql = new SqlExecute("sp_Student", _connectionString);

                SqlParameter key = new SqlParameter(@"Key", SqlDbType.VarChar, 50);
                key.Direction = ParameterDirection.Input;
                key.Value = "InsertStudent";
                sql.Parameters.Add(key);

                SqlParameter studentsID = new SqlParameter(@"STUDENT_ID", SqlDbType.VarChar, 10);
                studentsID.Direction = ParameterDirection.Input;
                studentsID.Value = requse.students_ID;
                sql.Parameters.Add(studentsID);

                if (ChackChar(requse.students_NAME) == true)
                {

                    SqlParameter studentsName = new SqlParameter(@"STUDENT_NAME", SqlDbType.VarChar, 50);
                    studentsName.Direction = ParameterDirection.Input;
                    studentsName.Value = requse.students_NAME;
                    sql.Parameters.Add(studentsName);
                }
                else if (ChackChar(requse.students_NAME) == false) {

                    response.message = "Name your not thai";
                }

                SqlParameter studentsPhone = new SqlParameter(@"STUDENT_PHONE", SqlDbType.VarChar, 10);
                studentsPhone.Direction = ParameterDirection.Input;
                studentsPhone.Value = requse.students_PHONE;
                sql.Parameters.Add(studentsPhone);
                /*if (ChackPhone(requse.students_PHONE) == true)
                {

                    SqlParameter studentsPhone = new SqlParameter(@"STUDENT_PHONE", SqlDbType.VarChar, 10);
                    studentsPhone.Direction = ParameterDirection.Input;
                    studentsPhone.Value = requse.students_PHONE;
                    sql.Parameters.Add(studentsPhone);
                }
                else if (ChackPhone(requse.students_PHONE) == false)
                {

                    response.message = "Phone your not ture";
                }*/

                if (ChackMail(requse.stadents_EMAIL) == true)
                {

                    SqlParameter studentsEmail = new SqlParameter(@"STUDENT_EMAIL", SqlDbType.VarChar, 50);
                    studentsEmail.Direction = ParameterDirection.Input;
                    studentsEmail.Value = requse.stadents_EMAIL;
                    sql.Parameters.Add(studentsEmail);
                }
                else if (ChackPhone(requse.stadents_EMAIL) == false)
                {

                    response.message = "Email your not ture";
                }

                table = sql.executeReturnManyTable()
 ;
                DataRow checkStatus = table.Tables[0].Rows[0];
                response.message = checkStatus["STATUS"].ToString();

            }
            catch (Exception ex) {

                response.message = ex.ToString();
            }

            return response;
        }

        public async Task<UtilityResponse<int>> EditStudent(EditStudent request) {

            UtilityResponse<int> response = new UtilityResponse<int>();

            try {

                DataSet table = new DataSet();
                SqlExecute sql = new SqlExecute("sp_Student", _connectionString);

                SqlParameter key = new SqlParameter(@"Key", SqlDbType.VarChar, 50);
                key.Direction = ParameterDirection.Input;
                key.Value = "EditStudent";
                sql.Parameters.Add(key);

                SqlParameter studentsID = new SqlParameter(@"STUDENT_ID", SqlDbType.VarChar, 10);
                studentsID.Direction = ParameterDirection.Input;
                studentsID.Value = request.students_ID;
                sql.Parameters.Add(studentsID);

                if (ChackChar(request.students_NAME) == true)
                {

                    SqlParameter studentsName = new SqlParameter(@"STUDENT_NAME", SqlDbType.VarChar, 50);
                    studentsName.Direction = ParameterDirection.Input;
                    studentsName.Value = request.students_NAME;
                    sql.Parameters.Add(studentsName);
                }
                else if (ChackChar(request.students_NAME) == false)
                {

                    response.message = "Name your not thai";
                }

                SqlParameter studentsPhone = new SqlParameter(@"STUDENT_PHONE", SqlDbType.VarChar, 10);
                studentsPhone.Direction = ParameterDirection.Input;
                studentsPhone.Value = request.students_PHONE;
                sql.Parameters.Add(studentsPhone);
                /*if (ChackPhone(requse.students_PHONE) == true)
                {

                    SqlParameter studentsPhone = new SqlParameter(@"STUDENT_PHONE", SqlDbType.VarChar, 10);
                    studentsPhone.Direction = ParameterDirection.Input;
                    studentsPhone.Value = requse.students_PHONE;
                    sql.Parameters.Add(studentsPhone);
                }
                else if (ChackPhone(requse.students_PHONE) == false)
                {

                    response.message = "Phone your not ture";
                }*/

                if (ChackMail(request.stadents_EMAIL) == true)
                {

                    SqlParameter studentsEmail = new SqlParameter(@"STUDENT_EMAIL", SqlDbType.VarChar, 50);
                    studentsEmail.Direction = ParameterDirection.Input;
                    studentsEmail.Value = request.stadents_EMAIL;
                    sql.Parameters.Add(studentsEmail);
                }
                else if (ChackPhone(request.stadents_EMAIL) == false)
                {

                    response.message = "Email your not ture";
                }

                table = sql.executeReturnManyTable()
 ;
                DataRow checkStatus = table.Tables[0].Rows[0];
                response.message = checkStatus["STATUS"].ToString();
            }
            catch (Exception ex) {

                response.message = ex.ToString();
            }
            return response;
        }

        public async Task<UtilityResponse<int>> Register(RegisterRequste request)
        {

            UtilityResponse<int> response = new UtilityResponse<int>();

            try
            {

                DataSet table = new DataSet();
                SqlExecute sql = new SqlExecute("sp_Student", _connectionString);

                SqlParameter key = new SqlParameter(@"Key", SqlDbType.VarChar, 50);
                key.Direction = ParameterDirection.Input;
                key.Value = "EditStudent";
                sql.Parameters.Add(key);

                SqlParameter studentsID = new SqlParameter(@"STUDENT_ID", SqlDbType.VarChar, 10);
                studentsID.Direction = ParameterDirection.Input;
                studentsID.Value = request.students_Id;
                sql.Parameters.Add(studentsID);

                SqlParameter studentsPhone = new SqlParameter(@"COURSE_CODE", SqlDbType.VarChar, 10);
                studentsPhone.Direction = ParameterDirection.Input;
                studentsPhone.Value = request.course_code;
                sql.Parameters.Add(studentsPhone);
               
                table = sql.executeReturnManyTable()
 ;
                DataRow checkStatus = table.Tables[0].Rows[0];
                response.message = checkStatus["STATUS"].ToString();
            }
            catch (Exception ex)
            {

                response.message = ex.ToString();
            }
            return response;
        }

        public async Task<UtilityResponse<int>> InsertCourse(InsertCourse request)
        {

            UtilityResponse<int> response = new UtilityResponse<int>();

            try
            {

                DataSet table = new DataSet();
                SqlExecute sql = new SqlExecute("sp_Student", _connectionString);

                SqlParameter key = new SqlParameter(@"Key", SqlDbType.VarChar, 50);
                key.Direction = ParameterDirection.Input;
                key.Value = "Insert_Course";
                sql.Parameters.Add(key);

                if (ChackENG(request.Course_Code) == true)
                {

                    SqlParameter Course_Code = new SqlParameter(@"COURSE_CODE", SqlDbType.VarChar, 50);
                    Course_Code.Direction = ParameterDirection.Input;
                    Course_Code.Value = request.Course_Code;
                    sql.Parameters.Add(Course_Code);
                }
                else if (ChackENG(request.Course_Code) == false)
                {

                    response.message = "Code not ture";
                }

                SqlParameter COURSENAME = new SqlParameter(@"COURSE_NAME", SqlDbType.VarChar, 50);
                COURSENAME.Direction = ParameterDirection.Input;
                COURSENAME.Value = request.Course_name;
                sql.Parameters.Add(COURSENAME);
              

                table = sql.executeReturnManyTable()
 ;
                DataRow checkStatus = table.Tables[0].Rows[0];
                response.message = checkStatus["STATUS"].ToString();

            }
            catch (Exception ex)
            {

                response.message = ex.ToString();
            }

            return response;
        }

        public async Task<UtilityResponse<int>> EditCourses(InsertCourse request)
        {

            UtilityResponse<int> response = new UtilityResponse<int>();

            try
            {

                DataSet table = new DataSet();
                SqlExecute sql = new SqlExecute("sp_Student", _connectionString);

                SqlParameter key = new SqlParameter(@"Key", SqlDbType.VarChar, 50);
                key.Direction = ParameterDirection.Input;
                key.Value = "Edit_Courses";
                sql.Parameters.Add(key);

                if (ChackENG(request.Course_Code) == true)
                {

                    SqlParameter Course_Code = new SqlParameter(@"COURSE_CODE", SqlDbType.VarChar, 50);
                    Course_Code.Direction = ParameterDirection.Input;
                    Course_Code.Value = request.Course_Code;
                    sql.Parameters.Add(Course_Code);
                }
                else if (ChackENG(request.Course_Code) == false)
                {

                    response.message = "Code not ture";
                }

                SqlParameter COURSENAME = new SqlParameter(@"COURSE_NAME", SqlDbType.VarChar, 50);
                COURSENAME.Direction = ParameterDirection.Input;
                COURSENAME.Value = request.Course_name;
                sql.Parameters.Add(COURSENAME);


                table = sql.executeReturnManyTable()
 ;
                DataRow checkStatus = table.Tables[0].Rows[0];
                response.message = checkStatus["STATUS"].ToString();

            }
            catch (Exception ex)
            {

                response.message = ex.ToString();
            }

            return response;
        }
        public async Task<SelectCoursResponse> SelectCourses()
        {

            SelectCoursResponse serviceResponse = new SelectCoursResponse();

            try
            {


                DataSet table = new DataSet();
                SqlExecute sql = new SqlExecute("sp_Student", _connectionString);

                SqlParameter key = new SqlParameter(@"Key", SqlDbType.VarChar, 50);
                key.Direction = ParameterDirection.Input;
                key.Value = "Selec_Courses";
                sql.Parameters.Add(key);

                table = sql.executeReturnManyTable();

                DataRow checkStatus = table.Tables[0].Rows[0];
                if (CheckNull(checkStatus["STATUS"].ToString()).Equals("SUCCESS"))
                {

                    if (table != null && table.Tables[1].Rows.Count > 0)
                    {
                        SelectCoursResponse.Courses courses = new SelectCoursResponse.Courses();
                        serviceResponse.courses = new List<SelectCoursResponse.Courses>();

                        foreach (DataRow row in table.Tables[1].Rows)
                        {
                            courses = new SelectCoursResponse.Courses();

                            courses.Course_Code = CheckNull(row["Course_Code"].ToString());
                            courses.Course_name = CheckNull(row["Course_name"].ToString());
                            courses.Student_Count = Convert.ToInt32(row["Student_Re"]);

                            serviceResponse.courses.Add(courses);

                        }
                        
                    }
                    
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return serviceResponse;
        }

        private bool ChackChar(string Value) {

            bool thai = false;
            foreach( char c in Value)
            {
                if ((int)c >= 161) thai = true;

            }
            return thai;
        }

        private bool ChackENG(string Value)
        {

            bool isValid = false;
            foreach (char c in Value)
            {
                if ((((int)c >= 65 && (int)c <= 90) || ((int)c >= 97 && (int)c <= 122)) || ((int)c >= 48 && (int)c <= 57)) isValid = true;

            }
            return isValid;
        }

        private bool ChackPhone(string Value)
        {
            bool isValid = false;
            if (!string.IsNullOrWhiteSpace(Value))
            {
                isValid = Regex.IsMatch(Value, @"/^(0[689]{1})+([0-9]{8})+$/g",
                     RegexOptions.IgnoreCase);
            }

            return isValid;
        }

        private bool ChackMail(string Value)
        {
            bool isValid = false;
            isValid = Value.EndsWith("@msu.ac.th", System.StringComparison.CurrentCultureIgnoreCase);

            return isValid;
        }

        private object ValueOrNull(object value)
        {
            return value ?? DBNull.Value;
        }

        private string CheckNull(string value)
        {
            return value ?? "";
        }


        

    }
}
