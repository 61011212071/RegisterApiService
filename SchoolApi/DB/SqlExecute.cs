﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolApi.DB
{
    public class SqlExecute
    {
        public List<SqlParameter> Parameters;
        private string sqlCommand;
        private string dbConnection;

        public SqlExecute(string command, string connectionString) {
            sqlCommand = command;
            dbConnection = connectionString;
            Parameters = new List<SqlParameter>();
        }

        public DataSet executeReturnManyTable()
        {
            DataSet result = new DataSet();
            using (SqlConnection connection = new SqlConnection(dbConnection))
            {
                using (SqlCommand cmd = new SqlCommand(sqlCommand, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (Parameters != null)
                        foreach (SqlParameter parameter in Parameters)
                            cmd.Parameters.Add(parameter);

                    try
                    {
                        if (connection.State == ConnectionState.Closed) connection.Open();

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(result);
                        }
                    }
                    catch (Exception e)
                    {
                        result = null;
                    }
                }
            }

            return result;
        }
    }
}
